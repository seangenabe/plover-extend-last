# plover-extend-last

Give more power to your last stroke!

## retro_last meta

- `{:retro_last:n}`
- `{:retro_last:n}`
- `{:retro_last:n,a,b,c,d}`

Retroactively surround your second to the last word with your last word. Use
repeatedly to surround more words.

Pass `n` to surround an additional _n_ items on the first stroke.

If your last word is the **closing version** of a bracket pair --
`)`, `]`, `}` -- using `retro_last` will insert the opening version of that
pair.

Pass a dictionary of items as a flat list of items separated by a comma (,) to
pass your own dictionary.

Supports repetitions of any character in the provided dictionary. (Single
character only.)

For now, only symbols are supported.

## surround_next macro

- `=surround_next`
- `=surround_next:a,b,c,d`

Repeats the last stroke, and presses left for every character in its
translation.

If your last word is the **opening version** of a bracket pair --
`(`, `[`, `{` -- using `surround_next` will insert the closing version of that
pair.

Pass a dictionary of items as a flat list of items separated by a comma (,) to
pass your own dictionary.
