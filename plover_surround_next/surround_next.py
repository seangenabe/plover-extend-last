from plover.translation import (
  Translator,
  Stroke,
  Translation
)
from pprint import pprint
from plover.formatting import _Context, _Action, RetroFormatter
from plover_retro_last.brackets import brackets

def surround_next(translator: Translator, stroke: Stroke, arg: str):
  translations = translator.get_state().translations
  args = arg.split(",")
  local_brackets = dict(
    list(
      zip(*[iter(args[1:])]*3)
    )
  ) if len(args) > 0 and len(args[0]) != 0 else brackets

  if not translations or len(translations) == 0:
    return

  translation: Translation = translations[-1]

  # Do not allow consecutive invocations if possible
  if len(translation.strokes) == 1 and stroke == translation.strokes[-1]:
    return

  # Get number of characters in translation
  formatter = RetroFormatter(translations)
  start_word = formatter.last_words(1)[0]
  end_word = create_end_word(start_word, local_brackets)

  # Repeat last translation with backtrack
  translation_copy = Translation(
    translation.strokes,
    end_word + ("{#Left}" * len(end_word))
  )
  translator.translate_translation(translation_copy)

def create_end_word(start_word: str, d: dict) -> str:
  if start_word in d:
    return d[start_word]

  if start_word[0] in d and all(c == start_word[0] for c in start_word[1:]):
    return d[start_word[0]] * len(start_word)

  return start_word

def p(name, var):
  print(name, end=" ")
  pprint(var)
