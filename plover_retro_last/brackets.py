brackets = {
  "(": ")",
  "[": "]",
  "{": "}",
  "“": "”",
  "‘": "’",
  "<": ">",
  "«": "»"
}

inv_brackets = { v: k for k, v in brackets.items() }
