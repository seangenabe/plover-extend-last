import itertools
from plover.formatting import _Context, _Action
from pprint import pprint
from plover_retro_last.brackets import inv_brackets

def retro_surround(context: _Context, arg: str) -> _Action:
  args = arg.split(",")
  n = max(
    (
      int(args[0])
      if len(args) > 0 and len(args[0]) != 0
      else getattr(context.last_action, "retro_last_count", 0)
    ),
    0
  )
  b = dict(
    list(
      zip(*[iter(args[1:])]*3)
    )
  )

  return _retro_surround(context, n, b)

def _retro_surround(
  context: _Context,
  n: int = 0,
  my_brackets=None
) -> _Action:
  action = context.copy_last_action()
  all_words_count = len(list(context.iter_last_words()))

  local_brackets = my_brackets if my_brackets else inv_brackets

  end_word = context.last_words(count = 1)[0]
  start_word = create_start_word(end_word, local_brackets)

  chomped_count = getattr(context.last_action, "retro_last_count", n)

  # Count the last word (1)
  # Count the additional word we'll add now (1)
  # We have captured these many words previously (n)
  # Count the word if we inserted it previously (1 if n > 0)
  get_words_count = min(
    2 + chomped_count + (1 if chomped_count > 0 else 0),
    # Do not chomp words beyond the word count
    all_words_count + 1
  )

  if get_words_count == all_words_count + 1:
    # Prevent users from chomping beyond word length
    # (+ 1 to make it possible to position the last word as the first word)
    action = context.new_action()
    action.word = "❌"
    action.text = "❌"
    action.prev_attach = True
    setattr(action, "retro_last_count", chomped_count)
    return action

  last_words_array = reverse_list(
    list(
      take(
        context.iter_last_words(),
        get_words_count
      )
    )
  )

  last_words = "".join(last_words_array)

  action.prev_replace = last_words

  if n == 0:
    action.text = start_word + last_words
  else:
    new_text_array = [start_word] + last_words_array[0:1] + last_words_array[2:]
    action.text = "".join(new_text_array)
  action.word = None
  action.prev_attach = True
  setattr(action, "retro_last_count", chomped_count + 1)

  return action

def create_start_word(end_word: str, d: dict) -> str:
  if end_word in d:
    return d[end_word]

  if end_word[0] in d and all(c == end_word[0] for c in end_word[1:]):
    return d[end_word[0]] * len(end_word)

  return end_word


def reverse_list(x):
  return [item for item in reversed(x)]

def take(x, n):
  return itertools.islice(x, n)

def p(name, var):
  print(name, end=" ")
  pprint(var)
